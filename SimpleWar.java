public class SimpleWar {
    public static void main(String[] args) {
		Deck drawPile = new Deck();
		drawPile.shuffle();
		
		int player1Pts = 0;
		int player2Pts = 0;
		
		while(drawPile.length() > 0) {
			Card player1Card = drawPile.drawTopCard();
			Card player2Card = drawPile.drawTopCard();
			
			System.out.println("-----------------------------------------------");
			
			System.out.println("Player 1: " + player1Card);
			System.out.println("Player 1: " + player1Card.calculateScore());
			
			System.out.println("\nPlayer 2: " + player2Card);
			System.out.println("Player 2: " + player2Card.calculateScore());
			
			if (player1Card.calculateScore() > player2Card.calculateScore()) {
				System.out.println("\nPlayer 1 gets the point !");
				player1Pts++;
			}
			if (player2Card.calculateScore() > player1Card.calculateScore()) { 
				System.out.println("\nPlayer 2 gets the point !");
				player2Pts++;
			}
		}
		
		System.out.println("************************************************************");
		
		if (player1Pts > player2Pts)
			System.out.println("Congrats player 1 won with " + player1Pts + " points !");
		else if (player2Pts > player1Pts)
			System.out.println("Congrats player 2 won with " + player2Pts + " points !");
		else
			System.out.println("It's a tie, better luck next time");
    }
}