import java.util.Random;

public class Deck {
    Card[] stack;
    int numOfCards;
    Random rng;

    public Deck() {
        rng = new Random();

        int[] value = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        String[] suit = new String[] {"hearts", "spades", "clubs", "diamonds"};
        this.stack = new Card[52];

        int i = 0;
        for(int v : value) {
            for(String s : suit) {
                stack[i] = new Card(s, v);
                i++;
            }
        }
        numOfCards = i;
    }

    public String toString() {
        String deck = "";

        for(Card card : stack) {
            deck += card.toString() + "\n";
        }
        return deck;
    }

    public int length() {
        return this.numOfCards;
    }

    public Card drawTopCard() {
        numOfCards--;
        return stack[numOfCards];
    }

    public void shuffle() {
        for(int i = 0; i < numOfCards; i++) {
            int randomIndex = i + rng.nextInt(numOfCards - i);
            Card tempCard = stack[randomIndex];
            stack[randomIndex] = stack[i];
            stack[i] = tempCard;
        }
    }
}
