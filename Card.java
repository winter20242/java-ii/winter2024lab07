public class Card {
    private String suit;
    private int value;

    public Card(String suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuit() {
        return this.suit;
    }

    public int getValue() {
        return this.value;
    }

    public String toString() {
        String printValue = Integer.toString(value);
        if (value == 1)
            printValue = "Ace";
        if (value == 11)
            printValue = "Jack";
        if (value == 12)
            printValue = "Queen";
        if (value == 13)
            printValue = "King";

        return printValue + " of " + suit;
    }
	
	public double calculateScore() {
		double score = 0;
		switch (this.suit) {
			case "hearts":
				score = value + 0.4;
				break;
			case "spades":
				score = value + 0.3;
				break;
			case "diamonds":
				score = value + 0.2;
				break;
			case "clubs":
				score = value + 0.1;
				break;
		}
		return score;
	}

}
